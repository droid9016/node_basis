class Logger {
    constructor(config){
        if(config === undefined)
            this.config = {};
        else
            this.config = config;

        this.getDateString = this.getDateString.bind(this);
        this.logRequest = this.logRequest.bind(this);
    }
    getDateString() {
        let d = new Date();
        switch(this.config.date){
            case 'y-m-d':
                return d.getFullYear() + "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) + "-" + 
                    ("00" + d.getDate()).slice(-2);
                break;
            case 'h:i:s':
                return ("00" + d.getHours()).slice(-2) + ":" + 
                    ("00" + d.getMinutes()).slice(-2) + ":" + 
                    ("00" + d.getSeconds()).slice(-2);
                break;
            case undefined:
                return d.getFullYear() + "-" +
                    ("00" + (d.getMonth() + 1)).slice(-2) + "-" + 
                    ("00" + d.getDate()).slice(-2) + " " + 
                    ("00" + d.getHours()).slice(-2) + ":" + 
                    ("00" + d.getMinutes()).slice(-2) + ":" + 
                    ("00" + d.getSeconds()).slice(-2);
                break;
            default:
                return 'Dateformat not supported';
        }
    }
    logRequest (req, res, next) {  
        console.log(this.getDateString(), req.method, req.originalUrl);
        next()
    }
    alert(msg){
        console.log("\x1b[31m%s\x1b[0m", this.getDateString()+' '+msg);
    }
    info(msg){
        console.log("\x1b[34m%s\x1b[0m", msg);
    }
    error(msg){
        console.log("\x1b[41m%s\x1b[0m", msg);
    }
}

export default new Logger;